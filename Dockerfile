FROM python:3.6.5

ENV PYTHONUNBUFFERED 1

RUN mkdir /code

WORKDIR /code

ADD . /code/

RUN pip install -r requirements.txt

ADD mysite/ /code/

CMD [ "python3", "manage.py", "runserver", "0.0.0.0:8000" ]
