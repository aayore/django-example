
First run (or after `docker-compose down -v`):
1. `docker-compose up` - This will start, but with errors
1. `docker exec django-example_web_1 python manage.py migrate` - Will update the DB with our Django models
1. `docker exec -it django-example_web_1 python manage.py createsuperuser` - To create the admin account
1. `docker-compose down && docker-compose up -d` - Restart to get settings changes

Subsequent runs:
1. `docker-compose down` - To bring down the containers and network
1. `docker-compose up -d` - This should start in the background, with persistent data

For testing:
1. `docker exec django-example_web_1 python manage.py test polls` - Runs the full suite of tests

When you're all done:
1. `docker-compose down -v` - Removes containers, network, and volumes
